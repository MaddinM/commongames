const { prefix, token, steamkey } = require("./config.json");
const steamProfileRegex = /https?:\/\/steamcommunity\.com\/profiles\/([\d]{17})\/?/;
const steamIdRegex = /https?:\/\/steamcommunity\.com\/id\/([\w]{2,})\/?/;

const Discord = require("discord.js");
const client = new Discord.Client();
client.login(token);

const fetch = require("node-fetch");

const { Client } = require('pg');

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setActivity("!!help");
});

client.on("message", msg => {
    // ignore bots
    if (msg.author.bot) return;
    // ignore messages without the prefix
    if(msg.content.indexOf(prefix) !== 0) return;

    const args = msg.content.slice(prefix.length).trim().split(' ');
    const command = args.shift().toLowerCase();

    if(command == "newdatabase") {
        (async () => {
            var postgres = new Client({connectionString: process.env.DATABASE_URL, ssl: { rejectUnauthorized: false }});
            await postgres.connect();
            await postgres.query(`CREATE TABLE IF NOT EXISTS GAMENAMES (GAME_ID TEXT NOT NULL, GAME_NAME TEXT, PRIMARY KEY(GAME_ID));`);
            await postgres.query(`CREATE TABLE IF NOT EXISTS USERGAMES (USER_ID TEXT NOT NULL, GAME_ID TEXT NOT NULL, PLAYTIME INTEGER NOT NULL, UNIQUE(USER_ID, GAME_ID));`);
            await postgres.query(`CREATE TABLE IF NOT EXISTS USERS (USER_ID TEXT NOT NULL, USER_NAME TEXT NOT NULL, STEAM_ID TEXT NOT NULL UNIQUE, PRIMARY KEY(USER_ID));`);
            await postgres.end();
        })();
    }

    if(command == "id") {
        (async () => {
            const query = {
                text: 'SELECT GAME_NAME FROM GAMENAMES WHERE GAME_ID = $1',
                values: [args[0]]
            }

            var postgres = new Client({connectionString: process.env.DATABASE_URL, ssl: { rejectUnauthorized: false }});
            await postgres.connect();

            const res = await postgres.query(query);
            if (res.rows.length === 0) {
                msg.channel.send("Steam ID not found!");
            } else {
                msg.channel.send(res.rows[0].game_name);
            }

            await postgres.end();
        })();
    }

    if(command == "add") {
        if (args.length === 1) {
            if (steamProfileRegex.test(args[0])) {
                var match = steamProfileRegex.exec(args[0]);
                confirmSteamIdAndAddToDatabase(msg, match[1]);
            } else if (steamIdRegex.test(args[0])) {
                var match = steamIdRegex.exec(args[0]);
                getSteamIdFromName(msg, match[1]);
            } else if (args[0].length === 17 && /^\d+$/.test(args[0])) {
                confirmSteamIdAndAddToDatabase(msg, args[0]);
            } else {
                getSteamIdFromName(msg, args[0]);
            }
        } else {
            msg.channel.send("Usage: !add <SteamID or SteamID64>");
        }
    }

    if(command == "sync") {
        sync(msg);
    }

    if(command == "common") {
        common(msg);
    }

    if(command == "help") {
        help(msg);
    }
});

async function help(msg) {
    const embed = new Discord.MessageEmbed().setTitle(`Help`);

    embed.addFields(
        { name: "!!add <SteamID, SteamID64 oder Link zu deinem Steam-Profil>", value: "Füge deinen Steam-Account hinzu, um zu synchronisieren." },
        { name: "!!sync", value: "Syn­chro­ni­sie­re deine Spiele, um diese zu vergleichen." },
        { name: "!!common", value: "Zeige die gemeinsamen Spiele an, die du mit den Leuten im gleichen Voicechannel hast. (Die anderen müssen auch synchronisiert sein.)" },
        { name: "!!id <Zahl>", value: "Zeige den Namen des Steam-Spiels zur gegebenen ID." },
    )

    msg.channel.send(embed);
}

async function sync(msg) {
    try {
        msg.channel.send("Starting sync! 🤞");
        var postgres = new Client({connectionString: process.env.DATABASE_URL, ssl: { rejectUnauthorized: false }});
        await postgres.connect();

        const steamIdQuery = {
            text: 'SELECT STEAM_ID FROM USERS WHERE USER_ID = $1',
            values: [msg.author.id]
        }
        const steamIdResult = await postgres.query(steamIdQuery);
        const steamId = await steamIdResult.rows[0].steam_id;

        const gamesFetch = await fetch(`http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${steamkey}&steamid=${steamId}&format=json`);
        const json = await gamesFetch.json();
        const games = await json.response.games;

        msg.channel.send(`Please wait, this will take about ${Math.round(games.length/4)} seconds! 🕑`)

        for (const game of games) {

            const userGameQuery = {
                text: 'INSERT INTO USERGAMES(USER_ID, GAME_ID, PLAYTIME) VALUES($1, $2, $3) ON CONFLICT DO NOTHING',
                values: [msg.author.id, game.appid, Math.round(game.playtime_forever/60)]
            }
            await postgres.query(userGameQuery);

            const gameColumn = await fetch(`http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key=${steamkey}&appid=${game.appid}`);
            const json = await gameColumn.json();
            const gameName = await json.game.gameName;

            const gameNameQuery = {
                text: 'INSERT INTO GAMENAMES(GAME_ID, GAME_NAME) VALUES($1, $2) ON CONFLICT DO NOTHING',
                values: [game.appid, gameName]
            }
            await postgres.query(gameNameQuery);
        }

        await postgres.end();
        msg.channel.send("Finished Sync! 👍");

    } catch(error) {
        console.log(error);
        msg.channel.send("Something went wrong! Check if your steam profile is not private! Otherwise <@184932878541389824> will look into a possible error.");
    }
}

async function common(msg) {
    try {
        if (msg.member.voice.channel == null) {
            return msg.channel.send("You have to be connected to a voice channel to compare games!");
        }

        if (msg.member.voice.channel.members.size === 1) {
            return msg.channel.send("You cannot compare games alone!");
        }

        var postgres = new Client({connectionString: process.env.DATABASE_URL, ssl: { rejectUnauthorized: false }});
        await postgres.connect();

        const userQuery = {
            text: `SELECT USER_ID, USER_NAME FROM USERS WHERE USER_ID 
            IN (${Array.from(msg.member.voice.channel.members.keys()).map(function(curr, i){ return '$'+(++i) }).join(',')})`,
            values: Array.from(msg.member.voice.channel.members.keys())
        }

        const usersInDatabase = await postgres.query(userQuery);

        const usersToCompare = usersInDatabase.rows.map(user => user.user_id);
        const userNames = usersInDatabase.rows.map(user => user.user_name);

        const commonQuery = {
            text: `SELECT GN.GAME_NAME, COUNT(UG.USER_ID), SUM(UG.PLAYTIME) FROM USERGAMES AS UG 
            INNER JOIN GAMENAMES AS GN ON UG.GAME_ID = GN.GAME_ID
            WHERE GN.GAME_NAME IS NOT NULL AND UG.USER_ID IN (${usersToCompare.map(function(curr, i){ return '$'+(++i) }).join(',')})
            GROUP BY GN.GAME_NAME HAVING COUNT(UG.USER_ID) = ${usersToCompare.length}
            ORDER BY SUM(UG.PLAYTIME) DESC LIMIT 12`,
            values: usersToCompare
        }

        const commonGames = await postgres.query(commonQuery);

        await postgres.end();

        if (commonGames.rows.length === 0) {
            return msg.channel.send("You don't have any games in common! ☹️");
        }

        const embed = new Discord.MessageEmbed()
        .setTitle(`Gemeinsame Spiele: ${userNames.toString()}`);

        commonGames.rows.forEach((commonGame) => {
            embed.addFields(
                { name: commonGame.game_name ? commonGame.game_name : "name_missing", value: commonGame.sum + " h", inline: true },
            )
        });

        msg.channel.send(embed);

    } catch(error) {
        console.log(error);
    }
}

function confirmSteamIdAndAddToDatabase(msg, steamId64) {
    msg.channel.send(`Is that your account? http://steamcommunity.com/profiles/${steamId64}`)
        .then(function (botReply) {
            botReply.react("✅");
            botReply.react("❌");

            const filter = (reaction, user) => (reaction.emoji.name === "✅" || reaction.emoji.name === "❌") && user.id === msg.author.id;

            let collector = botReply.createReactionCollector(filter, { time: 30000 });
            collector.on("collect", (reaction, collector) => {
                (async () => {
                    if (reaction.emoji.name === "✅") {
                        botReply.reactions.removeAll();

                        const query = {
                            text: 'INSERT INTO USERS(USER_ID, USER_NAME, STEAM_ID) VALUES($1, $2, $3) ON CONFLICT DO NOTHING',
                            values: [msg.author.id, msg.author.username, steamId64],
                        }

                        var postgres = new Client({connectionString: process.env.DATABASE_URL, ssl: { rejectUnauthorized: false }});
                        await postgres.connect();
                        await postgres.query(query);
                        msg.channel.send("Nice, you have been added. Your games can now be compared to other users! 🎉");
                        await postgres.end();

                    } else {
                        //UnhandledPromiseRejectionWarning
                        botReply.delete();
                        msg.channel.send("Okay, try again! You can find your ID on your steam profile URL! 🤓");
                    }
                })();
            });
            collector.on("end", collected => {
                botReply.delete();
            });
        });
}

function getSteamIdFromName(msg, steamName) {
    (async () => {

        const userFetch = await fetch(`http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=${steamkey}&vanityurl=${steamName}`);
        const json = await userFetch.json();

        if (json.response.success === 1) {
            const steamId = await json.response.steamid;
            confirmSteamIdAndAddToDatabase(msg, steamId);
        } else {
            msg.channel.send(`Could not find steam account with id '${steamName}'`);
        }
    })();
}